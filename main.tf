data "google_project" "project" {
  provider = google-beta
  project_id = var.gcp_project_id
}

data "google_project" "host_project"{
  provider = google-beta
  project_id = var.gcp_host_project_id
}

data "google_compute_network" "tf-vpc"{
  name = "tf-vpc"
  project = var.gcp_host_project_id
}

data "google_compute_subnetwork" "sub-uscentral1"{
  name = "sub-uscentral1"
  project = var.gcp_host_project_id
}

data "google_compute_global_address" "tf-psa-ip" {
  name = "tf-psa-ip"
  project = var.gcp_host_project_id
}

resource "google_project_service" "gcp_services" {
  for_each = toset(var.gcp_service_list)
  project = var.gcp_project_id
  service = each.key
}


# resource "google_service_account" "sa" {
#   account_id   = "tf-sa-test"
#   display_name = "test sa"
# }

# resource "google_project_iam_member" "sa" {
#   project   = var.gcp_project_id
#   role      = "roles/aiplatform.user"
#   member    = "serviceAccount:${google_service_account.sa.email}"

#   depends_on = [ 
#     google_service_account.sa
#   ]
# }


resource "random_id" "db_name_suffix" {
  byte_length = 4
}

resource "google_sql_database_instance" "instance" {
  provider = google-beta

  name             = "private-instance-${random_id.db_name_suffix.hex}"
  region           = var.gcp_region
  database_version = "POSTGRES_15"
  project          = var.gcp_project_id
  deletion_protection = false

  settings {
    tier = "db-f1-micro"
    availability_type = "REGIONAL"
    #edition = "ENTERPRISE_PLUS"
    ip_configuration {
      ipv4_enabled                                  = false
      private_network                               = data.google_compute_network.tf-vpc.id
      enable_private_path_for_google_cloud_services = true
    }
    backup_configuration{
      enabled = true
      point_in_time_recovery_enabled = true
    }
  }
}

resource "google_sql_database_instance" "replica" {
  provider = google-beta

  name             = "private-instance-replica-${random_id.db_name_suffix.hex}"
  master_instance_name = google_sql_database_instance.instance.name
  region           = var.gcp_region
  database_version = "POSTGRES_15"
  project          = var.gcp_project_id
  deletion_protection = false

  settings {
    tier = "db-f1-micro"
    availability_type = "REGIONAL"
    #edition = "ENTERPRISE_PLUS"
    ip_configuration {
      ipv4_enabled                                  = false
      private_network                               = data.google_compute_network.tf-vpc.id
      enable_private_path_for_google_cloud_services = true
    }
  }
}

resource "google_service_account" "default" {
  account_id   = "service-account-gke"
  display_name = "Service Account"
}

resource "google_container_cluster" "primary" {
  name               = "gke-cluster"
  location           = var.gcp_region
  initial_node_count = 1
  remove_default_node_pool = true
  network            = data.google_compute_network.tf-vpc.id
  subnetwork         = data.google_compute_subnetwork.sub-uscentral1.id
  project            = var.gcp_project_id
  ip_allocation_policy {
    cluster_secondary_range_name  = "pod"
    services_secondary_range_name = "service"
  }
  private_cluster_config {
    enable_private_nodes = true
    enable_private_endpoint = false
    master_ipv4_cidr_block = "172.16.0.0/28"
  }
  node_config {
    # Google recommends custom service accounts that have cloud-platform scope and permissions granted via IAM Roles.
    service_account = google_service_account.default.email
    oauth_scopes = [
      "https://www.googleapis.com/auth/cloud-platform"
    ]
    labels = {
      foo = "bar"
    }
    tags = ["foo", "bar"]
  }
  timeouts {
    create = "30m"
    update = "40m"
  }
}

resource "google_container_node_pool" "primary_preemptible_nodes" {
  name       = "my-node-pool"
  location   = var.gcp_region
  cluster    = google_container_cluster.primary.name
  node_count = 1

  node_config {
    preemptible  = true
    machine_type = "e2-medium"

    # Google recommends custom service accounts that have cloud-platform scope and permissions granted via IAM Roles.
    service_account = google_service_account.default.email
    oauth_scopes    = [
      "https://www.googleapis.com/auth/cloud-platform"
    ]
  }
}